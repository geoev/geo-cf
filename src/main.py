import os
import json
import base64
import functions_framework
import sqlalchemy as sa
import psycopg2
import google.cloud.secretmanager as secrets


global_log_fields = {}
def log(msg, severity="NOTICE", **components):
    log_entry = dict(
        severity=severity,
        message=msg,
        **components,
        **global_log_fields
    )
    print(json.dumps(log_entry))

crdb_cert_path = os.path.abspath(os.environ['CRDB_CERT_PATH'])
log(F"cert path: {crdb_cert_path} (exists={os.path.exists(crdb_cert_path)}")
if not os.path.exists(crdb_cert_path):
    log('getting crdb cert...')
    project_id = os.environ['PROJECT_ID']
    client = secrets.SecretManagerServiceClient()
    name = F"projects/{project_id}/secrets/cockroach_db_cert/versions/1"
    response = client.access_secret_version(name=name)
    cert_contents = response.payload.data.decode('utf-8')
    log('crdb cert: yay!')

    cert_dir = os.path.dirname(crdb_cert_path)
    if not os.path.exists(cert_dir):
        log(f'creating {cert_dir}')
        os.makedirs(cert_dir)
    try:
        with open(crdb_cert_path, 'w') as cfile:
            print(cert_contents, file=cfile)
            log(F"{crdb_cert_path} written (len={len(cert_contents)})")
    except OSError as e:
        log(F"unable to write {crdb_cert_path}: {e}", severity='ERROR')

db_uri = os.environ['DATABASE_URI']
db = None
try:
    db = sa.create_engine(db_uri)
    log("connected to db (sorta...)")
except psycopg2.OperationalError as e:
    log(F"unable to connect to db!: {e}")

@functions_framework.cloud_event
def main(event, context=None):
    '''
    '''
    log(F"type(event.data) {type(event.data)}")
    log(F"event.data: {event.data}")
    try:
        for key, val in event.data.items():
            log(F"event.data[{key}]={val}")
    except Exception as e:
        log(F"2. caught {type(e)}: {e}")

    # log(F"context: {context}")
    # try:
    #     if context is None:
    #         context = get_context(event)
    # except Exception as e:
    #     print(F"3. caught {type(e)}: {e}")

    # query = sa.text("SELECT COUNT(*) FROM geo_event")
    try:
        data = get_data(event)
        log(f'got data!!! {data}')
    except Exception as e:
        print(F"4 (no data :( ). caught {type(e)}: {e}")
        data = None

    if data:
        query = """
INSERT INTO geo_event
        (lat, lon, ev_type, key, value, src)
        VALUES (:lat, :lon, :ev_type, :key, :value, :src)
"""
        try:
            db.execute(sa.text(query), **data)
            log("event inserted")
        except Exception as e:
            log(F"insert: caught {type(e)}: {e}")
            return 400
    else:
        log("no data to insert???")
    return 200

def get_context(event):
    return event.data['context']

def get_data(event):
    try:
        return event.data['data']
    except Exception:
        pass

    message = event.data['message']
    raw_data = message['data']
    log(F"got message[data]: type={type(raw_data)}")
    data = base64.b64decode(raw_data).decode()
    log(F"got decoded data: {data} ({type(data)})")
    try:
        return json.loads(data)
    except Exception:
        return data

