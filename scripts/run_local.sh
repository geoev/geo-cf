#!/bin/bash

set -eu

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
export ROOT=$(dirname ${SCRIPT_DIR})
cd ${ROOT}

source scripts/env_prepare.sh

cd ${ROOT}/src
SOURCE=main.py
TARGET=main
PORT=8080
SIGNATURE_TYPE=cloudevent
functions-framework --target=${TARGET} --port=${PORT} --signature-type=${SIGNATURE_TYPE} --debug
