#!/bin/bash

set -eu

env=$1
cd environments/$env
sops --encrypt --gcp-kms $KMS_PATH env.yml >env.sops.yml
git add env.sops.yml
echo $env/env.sops.yml encrypted

